**unreleased**
**1.1.6**

- Add line break

**1.1.5**

- Add only last section of the CHANGES

**1.1.4**

- Add CHANGES.md to release description

**1.1.3**

- Another try to get gitlab-release running

**1.1.2**

- Set tags to protected or variables unprotected

**1.1.1**

- Fix release upload 

**1.1.0**

- Add gitlab-release
- Add pypi release

**1.0.1**

- Add requirement.txt example

**1.0.0**

- Add presi also to pages
- Add LICENSE file

**0.1.1**

- Add git push && git push --tags

**0.1.0**

- Update docs reflect new structure
- Add a CHANGES.md

**0.0.2**

- Added Dockerfile [jod]
- Added pip-tools
- Added gitlab registry
